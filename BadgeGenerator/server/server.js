var http = require('http');
var formidable = require('formidable');
var fs = require('fs');
var pathh = require("path");
var url = require('url');
var qs = require('querystring');
var xml2js = require('xml2js');
var json2xml = require('json2xml');
var randomstring = require("randomstring");
var NodeSession = require('node-session');
var csv = require('csvtojson');
var nodemailer = require('nodemailer');
var rimraf = require('rimraf');

//var path = 'D:\\Facultate\\An2\\Semestrul2\\TW\\Proiect\\BadgeGenerator\\uploadedFiles\\data.xml';

fs.readFile('config.txt', 'utf8', function (err,data) {
  if (err) 
  {
    throw err;
  }
  session = new NodeSession({secret: data});
});

var path = 'data.xml';
var dirName;
var userConfig;
var code = '3210';

http.createServer(function (req, res) {

    session.startSession(req, res, function(){

    dirName = req.session.get('dir');

    res.setHeader('Access-Control-Allow-Origin', '*');
    var uri = url.parse(req.url).pathname;
    var filename = pathh.join(pathh.join(process.cwd(), 'public'), uri);

    fs.exists(filename, function (exists) {
        if (!exists) {
            if (req.url === '/firstFormFile') {
                var form = new formidable.IncomingForm();
                form.parse(req, function (err, fields, files) {
                    var tempPath = files.dataFile ? files.dataFile.path : undefined;
                    //var newPath = 'D:\\Facultate\\An2\\Semestrul2\\TW\\Proiect\\BadgeGenerator\\uploadedFiles\\' + files.dataFile ? files.dataFile.name : undefined;
                    var extension = fields.fileType;
                    var newPath = files.dataFile ? './public/users/' + dirName + '/users.' + extension : undefined;

                    console.log(extension);

                    fs.copyFile(tempPath, newPath, function (err) {
                        if (err) {
                            res.writeHead(301, {
                                Location: '/html/firstForm.html'
                            });
                        } else {

                            if (extension == 'xml')
                            {
                                ///////////////////////////
                                append(userConfig, '0');
                                ///////////////////////////

                                var parser = new xml2js.Parser();
                                fs.readFile(__dirname + '/public/users/' + dirName + '/users.xml', function(err, data) 
                                {   
                                    parser.parseString(data, function (err, result) 
                                    {
                                        for (key in result.root)
                                        {
                                            append(path, '<' + key + '>' + result.root[key] + '</' + key + '> \n');
                                            //console.log('XML Data: ' +  '<' + key + '>' + result.root[key] + '</' + key + '> \n');
                                        }

                                        append(path, '</data>');

                                        fs.readFile(userConfig, function(err, data) {
                                            if (data == code)
                                            {
                                                 generateHTML();

                                                res.writeHead(301, {
                                                    Location: '/users/' + dirName + '/htmlOutput.html'
                                                });
                                            }
                                            else
                                            {
                                                res.writeHead(301, {
                                                    Location: '/html/index.html'
                                                });
                                            }
                                        });
                                        res.end();
                                    });
                                });
                            }
                            else if (extension == 'csv')
                            {
                                ///////////////////////////
                                append(userConfig, '0');
                                ///////////////////////////

                                csvFilePath = './public/users/' + dirName + '/users.csv';

                                csv().fromFile(csvFilePath).then((jsonObj)=>{
                                    var obj = jsonObj[0];
           
                                    for (key in obj)
                                    {
                                        append(path, '<' + key + '>' + obj[key] + '</' + key + '> \n');
                                        console.log('XML Data: ' +  '<' + key + '>' + obj[key] + '</' + key + '> \n');
                                    }
                                    append(path, '</data>');

                                     fs.readFile(userConfig, function(err, data) {
                                        if (data == code)
                                        {
                                            generateHTML();

                                            res.writeHead(301, {
                                                Location: '/users/' + dirName + '/htmlOutput.html'
                                            });
                                        }
                                        else
                                        {
                                            res.writeHead(301, {
                                                Location: '/html/index.html'
                                            });
                                        }
                                    });
                                    res.end();
                                })
                            }
                        }
                    });
                });
            } else if (req.method === 'POST' && req.url === '/firstFormData') {
                ///////////////////////////
                append(userConfig, '0');
                ///////////////////////////

                var body = '';
                req.on('data', function (data) {
                    body += data;
                    if (body.length > 1e6)
                        req.connection.destroy();
                });

                req.on('end', function () {
                    var data = JSON.parse(body);
                    console.log(data);
                    append(path, '\n');
                    append(path, json2xml(data));
                    append(path, '\n');

                    console.log('Request on first form');
                    append(path, '</data>');

                    // req.session.flush();
                    // req.session.regenerate();

                   fs.readFile(userConfig, function(err, data) {
                    console.log('Data: ' + data + ' ' + code + '|' + (data == code));
                        if (data == code)
                        {
                            generateHTML();

                            res.writeHead(200, {
                                Location: '/users/' + dirName + '/htmlOutput.html'
                            });
                        }
                        else
                        {
                            res.writeHead(301, {
                                Location: '/html/index.html'
                            });
                        }
                        res.write('/users/' + dirName + '/htmlOutput.html');
                        res.end();
                    });
                });
            }
            else if(req.url === '/secondFormFile') {
                ///////////////////////////
                append(userConfig, '1');   
                ///////////////////////////

                var form = new formidable.IncomingForm();
                form.parse(req, function (err, fields, files) {
                    var tempPath = files.logo ? files.logo.path : undefined;
                    var newPath = './public/users/' + dirName + '/logo.png';
                    //console.log(files);
                    //console.log('Path: ' + dirName + '/' + files.logo.name);
                    fs.copyFile(tempPath, newPath, function (err) {
                        if (err) {
                            res.writeHead(301, {
                                Location: '/html/secondForm.html'
                            });
                        } else {
                          /*append logo */
                          res.writeHead(301, {
                            Location: '/html/firstForm.html'
                        });
                      }
                      res.end();
                  });
                });
            }
            else if (req.method === 'POST' && req.url === '/secondFormData') {
                ///////////////////////////
                append(userConfig, '2');
                ///////////////////////////

                var body = '';
                req.on('data', function (data) {
                    body += data;
                    if (body.length > 1e6)
                        req.connection.destroy();
                });

                req.on('end', function () {
                    var data = JSON.parse(body);
                    console.log(data);
                    append(path, '\n');
                    append(path, json2xml(data));

                    append(path, '\n');

                    console.log('Request on second form');
                    res.end();
                    return;
                });
            }
            else if(req.method === 'POST' && req.url === '/template') {

                if(!req.session.has('dir')) { // does not exist on session
                    dirName = randomstring.generate();
                    if(!fs.existsSync('./public/users')) {
                        fs.mkdirSync('./public/users');
                    }
                    req.session.put('dir', dirName);
                    fs.mkdirSync('./public/users/' + dirName);
                    path = './public/users/' + dirName + '/data.xml';
                    userConfig = './public/users/' + dirName + '/userConfig.txt';
                }

                ///////////////////////////
                fs.writeFileSync(userConfig, '3', function(err, file) {
                    if (err) throw err;
                });
                ///////////////////////////

                var body = '';
                req.on('data', function(data) {
                    body += data;
                    if (body.length > 1e6) {
                        req.connection.destroy();
                    }
                });

                req.on('end', function() {
                    var data = JSON.parse(body);
                    console.log(data);

                    fs.open(path, 'w', function(err, file) {
                        if (err) throw err;

                        append(file, '<?xml version = "1.0" encoding = "UTF-8"?>');
                        append(file, '\n');
                        append(file, '<data>');
                        append(file, '\n');
                        append(file, '<template>' + json2xml(data) + '</template> \n');
                        append(file, '\n');
                        console.log('Request on templates');
                        res.end();
                        return;
                    });
                });
            }
            else if(req.method === 'POST' && req.url === '/email') {

                var body = '';
                req.on('data', function(data) {
                    body += data;
                    if (body.length > 1e6) {
                        req.connection.destroy();
                    }
                });
console.log('From email: ' + json2xml(data) + ' ' + data);
                req.on('end', function() {
                    var data = JSON.parse(body);

                    var transporter=nodemailer.createTransport({
                        service: 'gmail',
                        auth: {
                            user: 'badgesender123@gmail.com',
                            pass: 'cartof123'
                        }       
                    });

                    var mailOptions = {
                        from: 'badgesender123@gmail.com',
                        to: json2xml(data),
                        subject: 'Your customized badge',
                        attachments: {
                            //path: './public/users/' + dirName +'/test.pdf'
                            path: './test.pdf'
                        },
                        text: 'Thank you for using our app!'
                    };

                    transporter.sendMail(mailOptions, function() {
                        console.log('Email sent!')
                    });

                    res.writeHead(200, {
                            Location: '/html/firstForm.html'
                        });
                    res.end();
                });
            }
        }
    else {
        try {
            if (fs.statSync(filename).isDirectory()) {
                filename += '/index.html';
            }
        } catch (e) {
                // nothing
            }

            fs.readFile(filename, function(err, file) {
                if (err) {
                    res.writeHead(500, {
                        'Content-Type': 'text/plain'
                    });
                    res.write(err + "\n");
                    res.end();
                    return;
                }
                res.writeHead(200);
                res.write(file);
                res.end();
            });
        }
    });
 })

}).listen(3000, function () {
    console.log('Server is running!');
});

function append(path, text) {
    fs.appendFileSync(path, text);
}

function generateHTML() 
{
    //var outputPath = './public/html/htmlOutput.html';
    var outputPath = './public/users/' + dirName + '/htmlOutput.html';
    var parser = new xml2js.Parser();

    fs.readFile('./public/html/header.html', function(err, data) {
         if (err) throw err;

        fs.writeFile('./public/users/' + dirName + '/header.html', data, function(err, file) {
                    if (err) throw err;
        });
    });

    fs.readFile('./public/html/footer.html', function(err, data) {
         if (err) throw err;

        fs.writeFile('./public/users/' + dirName + '/footer.html', data, function(err, file) {
                    if (err) throw err;
        });
    });

   

    fs.readFile(__dirname + '/' + path, function(err, data) 
    {
        parser.parseString(data, function (err, result) 
        {
            fs.writeFileSync(outputPath, '<!DOCTYPE html><html><head><title>Output</title>', function(err) 
                    {
                        if (err) 
                        {
                            throw err;
                        }
                    });

            append(outputPath, '<!DOCTYPE html><html><head><title>Output</title>');
            append(outputPath, '<link rel="stylesheet" type="text/css" href="../../css/style.css">');
            append(outputPath, '<meta name = "viewport" content="width=device-width, initial-scale=1.0">');
            append(outputPath, '<meta charset ="utf-8"></head>');
            append(outputPath, '<body>');
            append(outputPath, '<iframe class="iframe_header" src="header.html"></iframe>');
            append(outputPath, '<p id="outputText_2"> Your final result! </p>');
            append(outputPath, '<div id = "picture">');

            var template = result.data.template;

            for (var i = 0; i < result.data.noOfParticipants; i++)
            {
                var fname = eval(`result.data.fname${i}`);
                var lname = eval(`result.data.lname${i}`);
                var affiliation = eval(`result.data.affiliation${i}`);
                var status = eval(`result.data.statute${i}`);

                if (template == "template1")
                {
                    append(outputPath, '<div class = "templateLandscape_2 ' + result.data.backgroundColor + '"><div id = "template1_head_2"><img id = "logo_template1_2" src = "logo.png"></div><div id = "template1_data_2"><span class = "' + result.data.fontType + ' ' + result.data.fontSize + ' ' + result.data.fontColor + '">' + 'Name - <b>' + fname + ' ' + lname + '</b> <br> Affiliation - <b>' + affiliation + '</b> <br> Status - <b>' + status + '</b> </span></div><div id = "template1_obs_2"><span>' + result.data.eventName + '<br>' + result.data.eventLocation + '<br>' + result.data.eventDate + '<br>' + result.data.url + '</span></div></div>');
                }
                else if (template == "template2")
                {
                    append(outputPath, '<div class = "templateLandscape_2 ' + result.data.backgroundColor + '"><div id = "template2_head_2"><img id = "logo_template2_2" src = "logo.png"></div><div id = "template2_data_2"><span class = "' + result.data.fontType + ' ' + result.data.fontSize + ' ' + result.data.fontColor + '">' + 'Name - <b>' + fname + ' ' + lname + '</b> <br> Affiliation - <b>' + affiliation + '</b> <br> Status - <b>' + status + '</b> </span></div><div id = "template2_obs_2"><span>' + result.data.eventName + '<br>' + result.data.eventLocation + '<br>' + result.data.eventDate + '<br>' + result.data.url + '</span></div></div>');
                }
                else if (template == "template3")
                {
                    append(outputPath, '<div class = "templateLandscape_2 ' + result.data.backgroundColor + '"><div id = "template3_head_2"><img id = "logo_template3_2" src = "logo.png"></div><div id = "template3_data_2"><span class = "' + result.data.fontType + ' ' + result.data.fontSize + ' ' + result.data.fontColor + '">' + 'Name - <b>' + fname + ' ' + lname + '</b> <br> Affiliation - <b>' + affiliation + '</b> <br> Status - <b>' + status + '</b> </span></div><div id = "template3_obs_2"><span>' + result.data.eventName + '<br>' + result.data.eventLocation + '<br>' + result.data.eventDate + '<br>' + result.data.url + '</span></div></div>');
                }
                else if (template == "template4")
                {
                    append(outputPath, '<div class = "templatePortrait_2 ' + result.data.backgroundColor + '"><div id = "template4_head_2"><img id = "logo_template4_2" src = "logo.png"></div><div id = "template4_data_2"><span class = "' + result.data.fontType + ' ' + result.data.fontSize + ' ' + result.data.fontColor + '">' + 'Name - <b>' + fname + ' ' + lname + '</b> <br> Affiliation - <b>' + affiliation + '</b> <br> Status - <b>' + status + '</b> </span></div><div id = "template4_obs_2"><span>' + result.data.eventName + '<br>' + result.data.eventLocation + '<br>' + result.data.eventDate + '<br>' + result.data.url + '</span></div></div>');
                }
                else if (template == "template5")
                {
                    append(outputPath, '<div class = "templatePortrait_2 ' + result.data.backgroundColor + '"><div id = "template5_head_2"><img id = "logo_template5_2" src = "logo.png"></div><div id = "template5_data_2"><span class = "' + result.data.fontType + ' ' + result.data.fontSize + ' ' + result.data.fontColor + '">' + 'Name - <b>' + fname + ' ' + lname + '</b> <br> Affiliation - <b>' + affiliation + '</b> <br> Status - <b>' + status + '</b> </span></div><div id = "template5_obs_2"><span>' + result.data.eventName + '<br>' + result.data.eventLocation + '<br>' + result.data.eventDate + '<br>' + result.data.url + '</span></div></div>');
                }
                
                append(outputPath, '<br><br>');
            }

            append(outputPath, '</div>');

            append(outputPath, '<button class ="btn btn_central" onclick = "genPNG()"> Download as PNG </button>');
            append(outputPath, '<p class = "fontTypeL"> Or you can complete your e-mail address and we will send you a PDF file. </p>');
            append(outputPath, '<form> <input class="completeFormL widthForm" type ="text" id="email" name="email" placeholder="E-mail">');
            append(outputPath, '<input class = "btn btn_central" type = "button" value = "Send as PDF attachment" onclick = "submitEmail()"> </form>');

            append(outputPath, '<script type="text/javascript" src="../../javascript/firstForm.js"></script>');
            append(outputPath, '<script type="text/javascript" src="../../javascript/html2canvas.js"></script>');

            append(outputPath, '<iframe class = "iframe_footer" src = "footer.html"></iframe>');
            append(outputPath, '</div>');
            append(outputPath, '</body></html>');
        });
    });
    //fs.unlinkSync('/sessions/' + dirName);
}
  