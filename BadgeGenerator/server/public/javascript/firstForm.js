function goToPage(page)
{
	window.location.href = page;
}

function submitFirstFormData() {
	if(!validateFirstForm()) {
		alert('You have to complete all the fields!');
		return false;
	}
	var number = document.getElementById('noOfParticipants').value;
	var data = {
		noOfParticipants : number,
		eventName : document.getElementById('eventName').value,
		eventLocation : document.getElementById('eventLocation').value,
		eventDate : document.getElementById('eventDate').value
	};
	for(var i = 0; i < number; i++) {
		data['fname' + i] = document.getElementById('fname' + i).value;
		data['lname' + i] = document.getElementById('lname' + i).value;
		data['affiliation' + i] = document.getElementById('affiliation' + i).value;
		data['statute' + i] = document.getElementById('statute' + i).value;
	}
	var http = new XMLHttpRequest();
	http.open('POST', '/firstFormData', true);
	http.setRequestHeader('Content-Type', 'application/json');
	http.onreadystatechange = function() {
	    if(http.readyState == 4 && http.status == 200) {
	        window.location.href = this.responseText;
	    }
	}
	http.send(JSON.stringify(data));
}

function submitSecondFormData() {
	if(!validateSecondForm()) {
		alert('You have to complete all the fields!');
		return false;
	}
	var data = {
		backgroundColor : document.getElementById('selectBackColor').value,
		fontType : document.getElementById('selectFontType').value,
		fontSize : document.getElementById('selectFontSize').value,
		fontColor : document.getElementById('selectFontColor').value,
		url : document.getElementById('url').value
	};
	var http = new XMLHttpRequest();
	http.open('POST', '/secondFormData', true);
	http.setRequestHeader('Content-Type', 'application/json');
	http.onreadystatechange = function() {
	    if(http.readyState == 4 && http.status == 200) {
	        
	        window.location.href = '/html/secondForm.html';
	    }
	}
	http.send(JSON.stringify(data));
}

function submitTemplate() {
	var radios = document.getElementsByName('template');
	var templateID;
	for (var i = 0; i < radios.length; i++) {
	    if (radios[i].checked) {
	    	templateID = radios[i].value;
	    }
	}

	var http = new XMLHttpRequest();
	http.open('POST', '/template', true);
	http.setRequestHeader('Content-Type', 'application/json');
	http.onreadystatechange = function() {
	    if(http.readyState == 4 && http.status == 200) {
	        window.location.href = '/html/secondForm.html';
	    }
	}
	http.send(JSON.stringify(templateID));
}

function addFields() {
	var button = document.getElementById('submitFirstForm');
	button.classList.remove('hiddenL');
	var number = document.getElementById('noOfParticipants').value;
	var containerFields = document.getElementById('containerFields');
	while(containerFields.hasChildNodes()) {
		containerFields.removeChild(containerFields.lastChild);
	}
	for(var i = 0; i < number; i++) {
		var labelFirstName = document.createElement('label');
		labelFirstName.setAttribute('class', 'fontTypeL');
		labelFirstName.innerHTML = 'First Name';
		containerFields.appendChild(labelFirstName);

		var inputFirstName = document.createElement('input');
		inputFirstName.setAttribute('id', 'fname' + i);
		inputFirstName.setAttribute('type', 'text');
		inputFirstName.setAttribute('placeholder', 'First Name');
		inputFirstName.setAttribute('name', 'firstname' + i);
		containerFields.appendChild(inputFirstName);

		var labelLastName = document.createElement('label');
		labelLastName.setAttribute('class', 'fontTypeL');
		labelLastName.innerHTML = 'Last Name';
		containerFields.appendChild(labelLastName);

		var inputLastName = document.createElement('input');
		inputLastName.setAttribute('id', 'lname' + i);
		inputLastName.setAttribute('type', 'text');
		inputLastName.setAttribute('placeholder', 'Last Name');
		inputLastName.setAttribute('name', 'lastname' + i);
		containerFields.appendChild(inputLastName);

		var affiliationLabel = document.createElement('label');
		affiliationLabel.setAttribute('class', 'fontTypeL');
		affiliationLabel.innerHTML = 'Affiliation';
		containerFields.appendChild(affiliationLabel);

		var inputAffiliation = document.createElement('input');
		inputAffiliation.setAttribute('id', 'affiliation' + i);
		inputAffiliation.setAttribute('type', 'text');
		inputAffiliation.setAttribute('placeholder', 'Affiliation');
		inputAffiliation.setAttribute('name', 'affiliationParticipant' + i);
		containerFields.appendChild(inputAffiliation);

		var statuteLabel = document.createElement('label');
		statuteLabel.setAttribute('class', 'fontTypeL');
		statuteLabel.innerHTML = 'Statute';
		containerFields.appendChild(statuteLabel);

		var inputStatute = document.createElement('input');
		inputStatute.setAttribute('id', 'statute' + i);
		inputStatute.setAttribute('type', 'text');
		inputStatute.setAttribute('placeholder', 'Statute');
		inputStatute.setAttribute('name', 'statuteParticipant' + i);
		containerFields.appendChild(inputStatute);
	}
}

function validateFirstForm() {
	var eventName = document.getElementById('eventName').value;
	var eventLocation = document.getElementById('eventLocation').value;
	var eventDate = document.getElementById('eventDate').value;
	var number = document.getElementById('noOfParticipants').value;
	if(eventName === '') {
		return false;
	}
	if(eventLocation === '') {
		return false;
	}
	if(eventDate === '') {
		return false;
	}
	for(var i = 0; i < number; i++) {
		var fname = document.getElementById('fname' + i).value;
		var lname = document.getElementById('lname' + i).value;
		var affiliation = document.getElementById('affiliation' + i).value;
		var statute = document.getElementById('statute' + i).value;
		if(fname === '') {
			return false;
		}
		if(lname === '') {
			return false;
		}
		if(affiliation === '') {
			return false;
		}
		if(statute === '') {
			return false;
		}
	}
	return true;
}

function validateSecondForm() {
	var backgroundColor = document.getElementById('selectBackColor').value;
	var	fontType = document.getElementById('selectFontType').value;
	var	fontSize = document.getElementById('selectFontSize').value;
	var	fontColor = document.getElementById('selectFontColor').value;
	var	url = document.getElementById('url').value;
	if(backgroundColor === '') {
		return false;
	}
	if(fontType === '') {
		return false;
	}
	if(fontSize === '') {
		return false;
	}
	if(fontColor === '') {
		return false;
	}
	if(url === '') {
		return false;
	}
	return true;
}

function changeFileType() {
	var fileInput = document.getElementById('inputFile');
	var fileName = fileInput.value.split(/(\\|\/)/g).pop();
	console.log(fileName);
	var extension = fileName.split('.').pop();
	console.log(extension);
	var typeInput = document.getElementById('fileType');
	typeInput.value = extension;
}

function genPNG() {
	var div = document.getElementById("picture");
	html2canvas(div, {
		onrendered: function(canvas) {
			var img = canvas.toDataURL();
			// console.log(dataURL);
			downloadPNG(img, "test.png");
		}
	});
}

function downloadPNG(uri, name) {
	var link = document.createElement("a");
	link.href = uri;
	link.download = name;
	// document.body.appendChild(link);
	link.click();
}

function submitEmail() {
	var email = document.getElementsById('email').value;

	var http = new XMLHttpRequest();
	http.open('POST', '/email', true);
	http.setRequestHeader('Content-Type', 'application/json');
	http.onreadystatechange = function() {
	    if(http.readyState == 4 && http.status == 200) { 
	    	alert("Email sent!");
	    }
	}
	alert(email);
	http.send(JSON.stringify(email));
}
